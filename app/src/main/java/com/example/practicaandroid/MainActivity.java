package com.example.practicaandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.example.practicaandroid.adapters.ContactoAdapter;
import com.example.practicaandroid.entities.Contacto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rv = findViewById(R.id.rvContactos);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        List<Contacto> contactos = ListContactos();
        ContactoAdapter adapter = new ContactoAdapter(contactos);
        rv.setAdapter(adapter);

    }

    private List<Contacto> ListContactos() {
        List<Contacto> contactos = new ArrayList<>();

        contactos.add(new Contacto("Juan", "976555863"));
        contactos.add(new Contacto("Fiorella", "976845210"));
        contactos.add(new Contacto("Francisco", "976364875"));
        contactos.add(new Contacto("Freddy", "976897124"));
        contactos.add(new Contacto("Viviana", "976321598"));
        contactos.add(new Contacto("Cristina", "976742513"));
        contactos.add(new Contacto("Richard", "976854210"));
        contactos.add(new Contacto("Eduardo", "976854231"));
        contactos.add(new Contacto("Marcelo", "976857301"));
        contactos.add(new Contacto("Caleb", "976031023"));

        return contactos;
    }

}