package com.example.practicaandroid.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.practicaandroid.ListActivity;
import com.example.practicaandroid.MainActivity;
import com.example.practicaandroid.R;
import com.example.practicaandroid.entities.Contacto;

import org.w3c.dom.Text;

import java.util.List;

public class ContactoAdapter extends RecyclerView.Adapter<ContactoAdapter.ContactoViewHolder> {

    List<Contacto> contactos;

    public ContactoAdapter(List<Contacto> contactos) {this.contactos = contactos;}

    @Override
    public ContactoAdapter.ContactoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list, parent, false);

        return new ContactoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactoAdapter.ContactoViewHolder holder, int position) {

        View view = holder.itemView;

        Contacto contacto = contactos.get(position);

        TextView tvNombre = view.findViewById(R.id.tvNombre);
        TextView tvNumero = view.findViewById(R.id.tvNumero);
        Button btnLlamar = view.findViewById(R.id.btnLlamar);
        Button btnMensaje = view.findViewById(R.id.btnMensaje);

        tvNombre.setText(contacto.Nombre);
        tvNumero.setText(contacto.Numero);

        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("smsto:" + contacto.Numero)); // This ensures only SMS apps respond
                intent.putExtra("sms_body", "Hola mundo!!");
                view.getContext().startActivity(intent);
            }
        });

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", contacto.Numero, null));
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return contactos.size();
    }

    class ContactoViewHolder extends RecyclerView.ViewHolder{
        public ContactoViewHolder(View itemView) {
            super(itemView);
        }
    }
}
